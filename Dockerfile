FROM registry.gitlab.com/grahamcampbell/php:8.2-base

ENV XDEBUG_VERSION 3.4.0

RUN pecl install xdebug-$XDEBUG_VERSION \
    && docker-php-ext-enable xdebug
